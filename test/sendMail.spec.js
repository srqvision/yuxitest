
const Credentials = require ('../src/credentials.js');
const Google = require ('../src/pages/google.js');
const Login = require ('../src/pages/login.js');
const Gmail = require ('../src/pages/gmail.js');

beforeEach (() => {
    browser.ignoreSynchronization = true;
    browser.get("https://www.google.com.co");
    browser.driver.manage().deleteAllCookies();
});

describe('Send mail with Gmail', () => {

    it('Send mail with Gmail', () => {        
        Google.txtSearch.sendKeys("gmail").sendKeys(protractor.Key.ENTER);
        Google.txtFirstResult.click();        
        Login.txtUsername.sendKeys(Credentials.username).sendKeys(protractor.Key.ENTER);   
        browser.sleep(500);
        Login.txtPassword.sendKeys(Credentials.password).sendKeys(protractor.Key.ENTER);
        browser.sleep(5000);
        Gmail.btnRedactar.click();
        browser.sleep(1000);
        Gmail.txtTo.sendKeys(Credentials.username);
        Gmail.txtSubjectbox.sendKeys("test");
        Gmail.txtBodyMessage.sendKeys("test");
        Gmail.btnSendMessage.click();
        browser.sleep(500);
        Gmail.linkSeeMessage.click();
    });
});