
const Google = require ('../src/pages/google.js');

beforeEach (() => {
    browser.ignoreSynchronization = true;
    browser.get("https://www.google.com.co");
    browser.driver.manage().deleteAllCookies();
});

describe('Enter Gmail through Google search', () => {

    it('Enter to www.google.com and write in the search engine “Gmail” and press search and click first one result about Gmail', () => {        
        Google.txtSearch.sendKeys("gmail").sendKeys(protractor.Key.ENTER);
        Google.txtFirstResult.click();
    });

    it('Click another result about Gmail', () => {  
        Google.txtSearch.sendKeys("gmail").sendKeys(protractor.Key.ENTER);
        Google.txtOtherResult.click();
    });

});