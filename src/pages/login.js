'use strict';

class login {
    static get txtUsername () {
        return element(by.id('identifierId'));
    }

    static get txtPassword () {
        return element(by.name('password'));
    }

    static get btnNext () {
        return element(by.className('RveJvd snByac'));
    }
}

module.exports = login;