'use strict';

class google {
    static get txtSearch() {    
        return element(by.id('lst-ib'));
    }

    static get btnSearch() {        
        return element(by.name('btnK'));
    }

    static get txtFirstResult() {
        return element(by.linkText('Gmail - Google'));
    }

    static get txtOtherResult() {
        return element(by.linkText('Gmail - Aplicaciones de Android en Google Play'));
    }
}

module.exports = google;