'use strict';

class gmail {

    static get btnRedactar () {
        return element(by.xpath("//*[@class='z0']/div"));
    }

    static get txtTo () {
        return element(by.name('to'));
    }

    static get txtSubjectbox () {
        return element(by.name('subjectbox'));
    }

    static get txtBodyMessage () {
        return element(by.className('Am'));
    }

    static get btnSendMessage () {
        return element(by.xpath("//*[@class='J-J5-Ji btA']/div[contains(.,'Enviar')]"));
    }

    static get linkSeeMessage () {
        return element(by.id('link_vsm'));
    }

    static get lblMessageSend () {
        return element(by.xpath("//div[@class='ii gt ']//div[@dir]"))
    }
}

module.exports = gmail;