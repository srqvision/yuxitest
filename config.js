exports.config = {

    capabilities: {
        browserName: 'chrome',
        shardTestFiles: true,
        maxInstances: 1,
        chromeOptions: {
            args: [
                '--disable-infobars',
                '--disable-extensions',
                'verbose',
                '--incognito',
                '--test-type',
                '--ignore-certificate-errors',
                '--allow-running-insecure-content',
                '--disk-cache-dir=null',
                '--start-maximized',
                'log-path=/tmp/chromedriver.log'
            ]
        }
    },
  
    specs: ['test/*.spec.js'],
  
    baseUrl: 'https://www.google.com.co',

    onPrepare: function() {
        var SpecReporter = require('jasmine-spec-reporter');
        // add jasmine spec reporter
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));
    },
  
    framework: 'jasmine',
    allScriptsTimeout: 10000,
    getPageTimeout: 10000,
  
    jasmineNodeOpts: {
      onComplete: null,
      isVerbose: true,
      showColors: true,
      defaultTimeoutInterval: 500000,
      includeStackTrace: true,
      print: function() {}
    },
  };